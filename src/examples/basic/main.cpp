//#include <ESP8266WiFi.h>
//#include <IRCClient.h>
#include "config.h"
#include "WiFiNet.h"
#include "Sprocket.h"
#include "IrcPlugin.h"

#define IRC_SERVER "chat.freenode.net"
#define IRC_PORT 6665
#define IRC_NICKNAME "illucat"
#define IRC_USER "illucat"

WiFiNet *network;
Sprocket *sprocket;
IrcPlugin *irc;

void setup()
{

  delay(1000);

  sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
  irc = new IrcPlugin({IRC_SERVER, IRC_PORT, IRC_NICKNAME, IRC_USER});
  sprocket->addPlugin(irc);
  network = new WiFiNet(
      SPROCKET_MODE,
      STATION_SSID,
      STATION_PASSWORD,
      AP_SSID,
      AP_PASSWORD,
      HOSTNAME,
      CONNECT_TIMEOUT);
  network->connect();

  sprocket->activate();
  sprocket->publish("irc/connect", "");
  sprocket->publish("irc/join", "#illucat");
  sprocket->publish("irc/sendMessage", "hoiii");
}

void loop()
{
  sprocket->loop();
  yield();
}