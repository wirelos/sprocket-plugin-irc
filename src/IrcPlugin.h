#ifndef __IRC_PLUGIN__
#define __IRC_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <IRCClient.h>
#include <WiFiClient.h>
#include <Plugin.h>
#include "utils/print.h"
#include "IrcConfig.h"

using namespace std;
using namespace std::placeholders;

#define REPLY_TO     "mentex" // Reply only to this nick

class IrcPlugin : public Plugin
{
public:

  IrcPlugin(IrcConfig cfg);

  void activate(Scheduler *scheduler);

private:
  WiFiClient wifiClient;
  IRCClient* client;
  Task connectTask;
  Task processTask;
  String server;
  int port;
  String nick;
  String user;
  String channel;

  void applyConfig(IrcConfig cfg);
  void applyConfigFromFile(const char *fileName);
  void enableConnectTask(Scheduler *scheduler);
  void enableProcessTask(Scheduler *scheduler);
  virtual void connect();
  virtual void callback(IRCMessage ircMessage);
  virtual void debugSentCallback(String data);
  void join(String channel);
  void sendMessage(String msg);
  virtual int checkConfig();
};

#endif