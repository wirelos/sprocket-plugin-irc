#include "IrcPlugin.h"

IrcPlugin::IrcPlugin(IrcConfig cfg)
{
    applyConfig(cfg);
}

void IrcPlugin::applyConfig(IrcConfig cfg)
{
    server = String(cfg.server);
    port = cfg.port;
    nick = String(cfg.nickname);
    user = String(cfg.user);
}

void IrcPlugin::applyConfigFromFile(const char *fileName)
{
    IrcConfigJson configFile;
    configFile.fromFile(fileName);
    if (configFile.valid)
    {
        PRINT_MSG(Serial, "IRC", "apply config from file");
        applyConfig(configFile);
    }
}

void IrcPlugin::activate(Scheduler *scheduler)
{
    applyConfigFromFile("/ircConfig.json");
    client = new IRCClient(server.c_str(), port, wifiClient);
    client->setCallback(bind(&IrcPlugin::callback, this, _1));
    client->setSentCallback(bind(&IrcPlugin::debugSentCallback, this, _1));

    subscribe("irc/connect", bind(&IrcPlugin::connect, this));
    subscribe("irc/join", bind(&IrcPlugin::join, this, _1));
    subscribe("irc/sendMessage", bind(&IrcPlugin::sendMessage, this, _1));

    //enableConnectTask(scheduler);
    enableProcessTask(scheduler);
    PRINT_MSG(Serial, "IRC", "plugin activated");
}

void IrcPlugin::enableConnectTask(Scheduler *scheduler)
{
    connectTask.set(TASK_SECOND * 5, TASK_FOREVER, bind(&IrcPlugin::connect, this));
    scheduler->addTask(connectTask);
    connectTask.enable();
}

void IrcPlugin::enableProcessTask(Scheduler *scheduler)
{
    processTask.set(TASK_MILLISECOND * 5, TASK_FOREVER, bind(&IRCClient::loop, client));
    scheduler->addTask(processTask);
    processTask.enable();
}

int IrcPlugin::checkConfig(){
    int isValid = server.length() != 0 || port != 0 || nick.length() != 0 || user.length() != 0;
    publish("irc/configValid", String(isValid));
    return isValid;
}


void IrcPlugin::connect()
{
    if (checkConfig() && !client->connected())
    {
        PRINT_MSG(Serial, "IRC", String("Attempting connection to " + server + ":" + String(port)).c_str());
        PRINT_MSG(Serial, "IRC", String("NICK " + nick).c_str());
        PRINT_MSG(Serial, "IRC", String("USER " + user).c_str());
        if (client->connect(nick.c_str(), user.c_str()))
        {
            PRINT_MSG(Serial, "IRC", "connected");
            publish("irc/connected", "1");
        }
        else
        {
            PRINT_MSG(Serial, "IRC", "failed... ");
            publish("irc/connected", "0");
        }
        return;
    }
}

void IrcPlugin::join(String c)
{
    if (checkConfig() && client->connected())
    {

        channel = c;
        String joinCmd = "JOIN :" + channel + "\r\n";
        wifiClient.print(joinCmd.c_str());
        PRINT_MSG(Serial, "IRC", joinCmd.c_str());
    }
}

void IrcPlugin::sendMessage(String msg)
{
    if (checkConfig() && channel)
    {
        client->sendMessage(channel, msg);
    }
}

void IrcPlugin::callback(IRCMessage ircMessage)
{

    String message(ircMessage.nick + ":" + ircMessage.text);
    publish("irc/log", message);
    PRINT_MSG(Serial, "IRC", message.c_str());

    // PRIVMSG ignoring CTCP messages
    /* if (ircMessage.command == "PRIVMSG" && ircMessage.text[0] != '\001')
    {
        String message("<" + ircMessage.nick + "> " + ircMessage.text);
        PRINT_MSG(Serial, "IRC", message.c_str());

        if (ircMessage.nick == REPLY_TO)
        {
            client->sendMessage(ircMessage.nick, "Hi " + ircMessage.nick + "! I'm your IRC bot.");
        }

        return;
    } */
    PRINT_MSG(Serial, "IRC", ircMessage.original.c_str());
}

void IrcPlugin::debugSentCallback(String data)
{
    PRINT_MSG(Serial, "IRC", data.c_str());
}