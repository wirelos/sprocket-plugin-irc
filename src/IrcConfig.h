#ifndef __IRC_CONFIG__
#define __IRC_CONFIG__

#include <JsonStruct.h>

#define JSON_IRC_SERVER "ircServer"
#define JSON_IRC_PORT "ircPort"
#define JSON_IRC_NICKNAME "ircNickname"
#define JSON_IRC_USER "ircUser"

struct IrcConfig
{
    const char *server;
    int port;
    const char *nickname;
    const char *user;
};

struct IrcConfigJson : public IrcConfig, public JsonStruct
{
    void mapJsonObject(JsonObject &root)
    {
        root[JSON_IRC_SERVER] = server;
        root[JSON_IRC_PORT] = port;
        root[JSON_IRC_NICKNAME] = nickname;
        root[JSON_IRC_USER] = user;
    }
    void fromJsonObject(JsonObject &json)
    {
        if (!verifyJsonObject(json))
        {
            Serial.println("ERROR: cannot parse JSON object");
            valid = 0;
            return;
        }
        server = getAttr(json, JSON_IRC_SERVER, server);
        port = getIntAttrFromJson(json, JSON_IRC_PORT, port);
        nickname = getAttr(json, JSON_IRC_NICKNAME, nickname);
        user = getAttr(json, JSON_IRC_USER, user);
        valid = 1;
    };
};

#endif